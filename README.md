
#使用Unity发送邮件案例
-------------------

## 案例效果图
![这里写图片描述](http://img.blog.csdn.net/20160924121458121)
![这里写图片描述](http://img.blog.csdn.net/20160924121514599)
![发送之后立马收到邮件](http://img.blog.csdn.net/20160924121530169)
![收到的邮件](http://img.blog.csdn.net/20160924121542825)

## 代码

### CS控制台
#### 说明：单纯的发送邮件
``` C#
class Program
	{
		static void Main(string[] args)
		{
			SmtpClient mailClient = new SmtpClient("smtp.qq.com");
			mailClient.EnableSsl = true;
			//Credentials登陆SMTP服务器的身份验证.
			mailClient.Credentials = new NetworkCredential("1213250243@qq.com", "");
			//test@qq.com发件人地址、test@tom.com收件人地址
			MailMessage message = new MailMessage(new MailAddress("1213250243@qq.com"), new MailAddress("aladdingame@qq.com"));

			// message.Bcc.Add(new MailAddress("tst@qq.com")); //可以添加多个收件人
			message.Body = "Hello Word!";//邮件内容
			message.Subject = "this is a test";//邮件主题
			//Attachment 附件
			Attachment att = new Attachment(@"D:/test.mp3");
			message.Attachments.Add(att);//添加附件
			Console.WriteLine("Start Send Mail....");
			//发送....
			mailClient.Send(message);

			Console.WriteLine("Send Mail Successed");

			Console.ReadLine();
		}
	}
```
----------------
### Unity
#### 说明：截图并且发送到指定邮件
```C#
using UnityEngine;
using System.Collections;
using System;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

public class SendEmailSrc : MonoBehaviour
{
	void OnGUI()
	{
		if (GUI.Button(new Rect(0, 50, 100, 40), "Capture"))
		{
			Debug.Log("Capture Screenshot");
			Application.CaptureScreenshot("screen.png");
		}
		if (GUI.Button(new Rect(0, 0, 100, 40), "Send"))
		{
			SendEmail();
		}
	}

	private void SendEmail()
	{
		MailMessage mail = new MailMessage();

		mail.From = new MailAddress("1213250243@qq.com");
		mail.To.Add("1213250243@qq.com");
		mail.Subject = "Test Mail";
		mail.Body = "This is for testing SMTP mail from GMAIL";
		mail.Attachments.Add(new Attachment("screen.png"));

		SmtpClient smtpServer = new SmtpClient("smtp.qq.com");
		smtpServer.Credentials = new System.Net.NetworkCredential("1213250243@qq.com", "密码") as ICredentialsByHost;
		smtpServer.EnableSsl = true;
		ServicePointManager.ServerCertificateValidationCallback =
			delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
			{ return true; };

		smtpServer.Send(mail);
		Debug.Log("success");
	}
}
```
------------------
 
## [欢迎查看我的博客](http://blog.csdn.net/dingxiaowei2013/article/details/52649835)



----------

欢迎关注我的[围脖](http://weibo.com/dingxiaowei2013)

==================== 迂者 丁小未 CSDN博客专栏=================

 **我的QQ:1213250243** 
 **Unity QQ群：375151422**

====================== 相互学习，共同进步 ===================